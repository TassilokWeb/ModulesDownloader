﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Security.Policy;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for ModuleCollector
/// </summary>
public static class ModuleCollector
{

    public static DownloadPageModel PageModel { get; set; }

    public static DownloadPageModel GetModuleCode(string pathForDownloads)
    {
        PageModel = new DownloadPageModel(pathForDownloads);
        return PageModel;
    }
}