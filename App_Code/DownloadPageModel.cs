﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for DownloadPageModel
/// </summary>
public class DownloadPageModel
{
    public string Updated { get; private set; }
    public string Title { get; private set; }
    public string ModulesCode { get; private set; }
    public List<ModuleItem> ModuleItems { get; private set; }
    public string MainModule { get; private set; }
    public string DownloadBase { get; private set; }

    public DownloadPageModel(string path)
    {
        MainModule = "OntologyModule";
        DownloadBase = "http://localhost/BinariesDownloader/";
        Title = "OntologyModules";
        ModuleItems = new List<ModuleItem>();
        Updated = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ddZ");
        var regexVersion = new Regex(@"\d*\.\d*\.\d*\.\d*");
        var moduleId = 1;

        foreach (var directory in Directory.GetDirectories(path))
        {
            var directoryInfo = new DirectoryInfo(directory);
            var files = directoryInfo.GetFiles().OrderByDescending(f => f.CreationTime).ToList();
            foreach (var file in files)
            {
                if (Path.GetExtension(file.FullName).ToLower() == ".exe")
                {
                    var moduleItem = new ModuleItem();
                    var project = Path.GetFileName(directory);
                    moduleItem.Updated = Updated;
                    moduleItem.Id = moduleId;
                    moduleItem.Name = Path.GetFileName(directory);
                    var match = regexVersion.Match(file.FullName);
                    if (match.Success)
                    {
                        moduleItem.Version = match.Value;

                    }
                    else
                    {
                        moduleItem.Version = "-";
                    }

                    moduleItem.Url = DownloadBase + project + "/" + Path.GetFileName(file.FullName);

                    project = project.ToLower().Replace("-", "");
                    if (project == MainModule.ToLower())
                    {
                        moduleItem.OntologyModule = true;
                    }
                    ModuleItems.Add(moduleItem);
                    moduleId++;
                }
            }


        }
        ModulesCode = string.Join("\r\n", ModuleItems.OrderByDescending(module => module.OntologyModule).ThenBy(module => module.Id).Select(module => module.ModuleText));
    }
}