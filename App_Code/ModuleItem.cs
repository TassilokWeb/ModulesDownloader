﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ModuleItem
/// </summary>
public class ModuleItem
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool OntologyModule { get; set; }
    public string Version { get; set; }
    public string Url { get; set; }
    public string Updated { get; set; }

    public string ModuleText
    {
        get
        {
            return "<entry><title type=\"text\"></title><updated>" + Updated + "</updated><author><name /></author>" +
                    "<content type=\"application/xml\">\r\n" +
                    "\t<m:properties>\r\n" +
                    "\t\t<d:ModuleId>" + Id.ToString() + "</d:ModuleId>\r\n" +
                    "\t\t<d:Name>" + Name + "</d:Name>\r\n" +
                    "\t\t<d:Version>" + Version + " </d:Version>\r\n" +
                    "\t\t<d:Link>" + Url + "</d:Link>\r\n" +
                    "\t</m:properties>" +
                    "</content></entry>\r\n";
        }
    }
	
}